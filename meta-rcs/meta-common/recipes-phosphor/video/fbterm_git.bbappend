FILESEXTRAPATHS_prepend := "${THISDIR}/fbterm:"
SRC_URI += "file://fbterm-static-background.png"
SRC_URI += "file://fbterm.service"
SRC_URI += "file://fbterm-bi"

do_install_append() {
    install -d ${D}${systemd_system_unitdir}
    install -m 0644 ${WORKDIR}/fbterm.service ${D}${systemd_system_unitdir}

    install -d ${D}/usr/share/images/
    install -m 0644 ${WORKDIR}/fbterm-static-background.png  ${D}/usr/share/images/

    install -d ${D}/usr/bin/
    install -m 0755 ${WORKDIR}/fbterm-bi ${D}/usr/bin/
}

FILES_${PN} += " /usr/share/images/* /usr/bin/*"

RDEPENDS_${PN} += "bash fbv"
