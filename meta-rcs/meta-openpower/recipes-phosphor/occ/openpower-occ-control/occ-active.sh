#!/bin/sh
# set and unset occ active for all occ's in system

if [ "$1" == "disable" ]; then
    value='false'
elif [ "$1" == "enable" ]; then
    value='true'
elif [ "$1" == "watchdog" ]; then
    value='true'
else
    echo "Usage: occ-active.sh [argument]"
    echo "  enable  - set occ's to active state"
    echo "  disable - set occ's to inactive state"
    exit -1
fi

if [ "$value" == "true" ]; then
	# Verify host IPL is complete
	while [ ! -e /run/openbmc/host@0-ipl-complete ]; do
		echo "Waiting for host IPL to complete before engaging OCC interface..."
		sleep 1
	done
fi

# Get CPU count
CPU_COUNT=1
STATUS_FLAGS=$(i2cget -y 12 0x31 0x7)
if [ $? != 0 ]; then
	STATUS_FLAGS=$(i2cget -y 12 0x31 0x7)
fi
if [ $? != 0 ]; then
	STATUS_FLAGS=$(i2cget -y 12 0x31 0x7)
fi
CPU_PRESENT_FLAG_N=$(( ${STATUS_FLAGS} & 0x20 ))
if [ $CPU_PRESENT_FLAG_N != 0 ]; then
	CPU_COUNT=$(( ${CPU_COUNT} + 1 ))
fi
echo "Found $CPU_COUNT CPU(s)"

OCC_CONTROL_SERVICE="org.open_power.OCC.Control"

if [ "$value" == "true" ]; then
	# Disable existing OCC service
	busctl tree $OCC_CONTROL_SERVICE --list | grep occ | xargs -r -n1 -I{} \
		busctl set-property $OCC_CONTROL_SERVICE {} org.open_power.OCC.Status \
		OccActive b 'false'

	# Rescan bus to find OCCs
	openpower-proc-control scanFSI

	# Bind OCCs
	echo occ-hwmon.1 > /sys/bus/platform/drivers/occ-hwmon/bind
	if [ $CPU_COUNT -gt 1 ]; then
		echo occ-hwmon.2 > /sys/bus/platform/drivers/occ-hwmon/bind
	fi
fi

busctl tree $OCC_CONTROL_SERVICE --list | grep occ | xargs -r -n1 -I{} \
	busctl set-property $OCC_CONTROL_SERVICE {} org.open_power.OCC.Status \
	OccActive b $value

if [ "$value" == "true" ]; then
	# Wait 5 seconds before restarting fan controller
	sleep 5
	systemctl restart phosphor-fan-control@0.service
else
        # Unbind all OCC drivers to avoid bus contention on next IPL
	echo occ-hwmon.1 > /sys/bus/platform/drivers/occ-hwmon/unbind || true
	echo occ-hwmon.2 > /sys/bus/platform/drivers/occ-hwmon/unbind || true
	echo sbefifo1-dev0 > /sys/bus/platform/drivers/occ/unbind || true
	echo sbefifo2-dev0 > /sys/bus/platform/drivers/occ/unbind || true
fi

if [ "$1" == "watchdog" ]; then
	# Start watchdog
	while [ 1 == 1 ]; do
		sleep 5

		if [ ! -e /run/openbmc/host@0-ipl-complete ]; then
			echo "Exiting due to missing IPL complete flag file"
			exit 0
		fi

		OCC_FAULT=0
		if [ ! -e /sys/bus/platform/drivers/occ/sbefifo1-dev0/occ-hwmon.1/occ_error ]; then
			OCC_FAULT=1
		else
			STATUS=$(cat /sys/bus/platform/drivers/occ/sbefifo1-dev0/occ-hwmon.1/occ_error)
			if [ $STATUS != 0 ]; then
				OCC_FAULT=1
			fi
		fi
		if [ $CPU_COUNT -gt 1 ]; then
			if [ ! -e /sys/bus/platform/drivers/occ/sbefifo2-dev0/occ-hwmon.2/occ_error ]; then
				OCC_FAULT=1
			else
				STATUS=$(cat /sys/bus/platform/drivers/occ/sbefifo2-dev0/occ-hwmon.2/occ_error)
				if [ $STATUS != 0 ]; then
					OCC_FAULT=1
				fi
			fi
		fi

		if [ $OCC_FAULT != 0 ]; then
			# OCC driver failed.  Rescan bus to recover.
			# Disable existing OCC service
			busctl tree $OCC_CONTROL_SERVICE --list | grep occ | xargs -r -n1 -I{} \
				busctl set-property $OCC_CONTROL_SERVICE {} org.open_power.OCC.Status \
				OccActive b 'false'

			# Give enough time for the OCC control service to attempt unload before forcible unbind
			sleep 5

			# Unbind all OCC drivers
			echo occ-hwmon.1 > /sys/bus/platform/drivers/occ-hwmon/unbind || true
			echo occ-hwmon.2 > /sys/bus/platform/drivers/occ-hwmon/unbind || true
			echo sbefifo1-dev0 > /sys/bus/platform/drivers/occ/unbind || true
			echo sbefifo2-dev0 > /sys/bus/platform/drivers/occ/unbind || true

			# Wait 10 seconds for OCC to figure out something has happened / bus contention to stop
			sleep 10

			# Make sure system hasn't gone down in the interim
			if [ ! -e /run/openbmc/host@0-ipl-complete ]; then
				echo "Exiting due to missing IPL complete flag file"
				exit 0
			fi

			# Rescan bus to find OCCs
			openpower-proc-control scanFSI

			# Bind OCCs
			echo occ-hwmon.1 > /sys/bus/platform/drivers/occ-hwmon/bind
			if [ $CPU_COUNT -gt 1 ]; then
				echo occ-hwmon.2 > /sys/bus/platform/drivers/occ-hwmon/bind
			fi

			# Re-enable OCC service
			busctl tree $OCC_CONTROL_SERVICE --list | grep occ | xargs -r -n1 -I{} \
				busctl set-property $OCC_CONTROL_SERVICE {} org.open_power.OCC.Status \
				OccActive b 'true'

			# Give the OCC service time to start
			sleep 5

			# Restart fan controller
			systemctl restart phosphor-fan-control@0.service
		fi
	done
fi

exit 0
