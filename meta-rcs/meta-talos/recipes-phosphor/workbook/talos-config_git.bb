SUMMARY = "Talos II board wiring"
DESCRIPTION = "Talos II wiring information for the Talos II OpenPOWER system."
PR = "r1"
PV = "1.0+git${SRCPV}"

inherit config-in-skeleton
