SUMMARY = "Blackbird HDMI Setup"
DESCRIPTION = "Configure Blackbird HDMI transceiver"
PR = "r1"
LICENSE = "GPLv3"
LIC_FILES_CHKSUM = "file://COPYING.GPLv3;md5=d32239bcb673463ab874e80d47fae504"

inherit obmc-phosphor-systemd

RDEPENDS_${PN} += "i2c-tools bash"

S = "${WORKDIR}"
SRC_URI += "file://lvds.sh \
	    file://COPYING.GPLv3"

do_install() {
        install -d ${D}${bindir}
        install -m 0755 ${WORKDIR}/lvds.sh ${D}${bindir}/lvds.sh
}

TMPL = "lvds-config@.service"
INSTFMT = "lvds-config@{0}.service"
TGTFMT = "obmc-chassis-poweron@{0}.target"
FMT = "../${TMPL}:${TGTFMT}.requires/${INSTFMT}"

SYSTEMD_SERVICE_${PN} += "${TMPL}"
SYSTEMD_LINK_${PN} += "${@compose_list(d, 'FMT', 'OBMC_CHASSIS_INSTANCES')}"

