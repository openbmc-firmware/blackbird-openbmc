FILESEXTRAPATHS_append := ":${THISDIR}/${PN}"
SRC_URI_append = " \
	file://occ_sensors.hardcoded.yaml \
	file://hwmon_sensors.hardcoded.yaml \
	file://channel.yaml \
	"

EXTRA_OECONF_append = " \
        CHANNEL_YAML_GEN=${WORKDIR}/channel.yaml \
        "

# Replace the default whitelist on Blackbird systems
SRC_URI_append = " file://blackbird-ipmid-whitelist.conf"

WHITELIST_CONF_remove = " ${S}/host-ipmid-whitelist.conf"
WHITELIST_CONF_append = " ${WORKDIR}/blackbird-ipmid-whitelist.conf"
