SUMMARY = "Blackbird board wiring"
DESCRIPTION = "Blackbird wiring information for the Blackbird OpenPOWER system."
PR = "r1"
PV = "1.0+git${SRCPV}"

inherit config-in-skeleton
