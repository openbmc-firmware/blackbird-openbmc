FILESEXTRAPATHS_prepend := "${THISDIR}/${PN}:"

BLACKBIRD_CHIPS = " \
       pwm-tacho-controller@1e786000 \
       bus@1e78a000/i2c-bus@440/w83773g@4c \
       bus@1e78a000/i2c-bus@440/power-supply@68 \
       bus@1e78a000/i2c-bus@440/power-supply@69 \
       "
BLACKBIRD_ITEMSFMT = "ahb/apb/{0}.conf"
BLACKBIRD_ITEMS = "${@compose_list(d, 'BLACKBIRD_ITEMSFMT', 'BLACKBIRD_CHIPS')}"

BLACKBIRD_OCCS = " \
       00--00--00--06/sbefifo1-dev0/occ-hwmon.1 \
       "

BLACKBIRD_OCCSFMT = "devices/platform/gpio-fsi/fsi0/slave@00--00/{0}.conf"
BLACKBIRD_OCCITEMS = "${@compose_list(d, 'BLACKBIRD_OCCSFMT', 'BLACKBIRD_OCCS')}"

ENVS = "obmc/hwmon/{0}"
SYSTEMD_ENVIRONMENT_FILE_${PN} += "${@compose_list(d, 'ENVS', 'BLACKBIRD_ITEMS')}"
SYSTEMD_ENVIRONMENT_FILE_${PN} += "${@compose_list(d, 'ENVS', 'BLACKBIRD_OCCITEMS')}"
