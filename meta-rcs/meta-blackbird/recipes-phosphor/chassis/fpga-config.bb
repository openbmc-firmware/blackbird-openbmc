SUMMARY = "Blackbird FPGA Setup"
DESCRIPTION = "Configure Blackbird FPGA"
PR = "r1"
LICENSE = "GPLv3"
LIC_FILES_CHKSUM = "file://COPYING.GPLv3;md5=d32239bcb673463ab874e80d47fae504"

inherit obmc-phosphor-systemd

RDEPENDS_${PN} += "i2c-tools bash"

S = "${WORKDIR}"
SRC_URI += "file://fpga-setup.sh \
	    file://fpga-config.service \
	    file://COPYING.GPLv3"

do_install() {
        install -d ${D}${bindir}
        install -m 0755 ${WORKDIR}/fpga-setup.sh ${D}${bindir}/fpga-setup.sh
}

SYSTEMD_SERVICE_${PN} += "fpga-config.service"
