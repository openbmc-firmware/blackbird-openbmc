SUMMARY = "Simple binary block editor"

LICENSE = "GPLv2"
LIC_FILES_CHKSUM = "file://COPYING;md5=eb723b61539feef013de476e68b5c50a"

PE = "0"
PV = "0.2.2+git${SRCPV}"
SRCREV = "6c76ee16e1d4f1057ce37166001e95a87c5acb39"

SRC_URI = "git://scm.raptorcs.com/scm/git/binary-block-editor;branch=master;protocol=https"
S = "${WORKDIR}/git"

inherit autotools-brokensep

do_configure_prepend () {
    cd ${S}
    aclocal
    autoconf --force
    libtoolize --automake -c --force
    automake -ac
}

do_install () {
    install -d ${D}${bindir}
    install -m 755 ${S}/src/bbe ${D}${bindir}/bbe

    install -d ${STAGING_DIR}/bin
    install -m 755 ${S}/src/bbe ${STAGING_DIR}/bin/bbe
}
