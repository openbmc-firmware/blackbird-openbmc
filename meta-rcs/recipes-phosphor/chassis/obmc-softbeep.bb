SUMMARY = "OpenBMC software beep"
DESCRIPTION = "A sample implementation for software beep on a GPIO."
PR = "r1"

inherit skeleton-gdbus
inherit pkgconfig

LICENSE = "GPLv3"
SKELETON_DIR = "softbeep"
