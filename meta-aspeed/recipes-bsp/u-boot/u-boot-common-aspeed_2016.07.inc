HOMEPAGE = "https://github.com/openbmc/u-boot"
SECTION = "bootloaders"
DEPENDS += "flex-native bison-native"

LICENSE = "GPLv2+"
LIC_FILES_CHKSUM = "file://Licenses/README;md5=a2c678cfd4a4d97135585cad908541c6"
PE = "1"

# We use the revision in order to avoid having to fetch it from the
# repo during parse
SRCREV = "628ade99494153a86e5e6490166692009f30319f"

UBRANCH = "v2016.07-aspeed-openbmc"
SRC_URI = "git://scm.raptorcs.com/scm/git/blackbird-obmc-uboot;branch=${UBRANCH}-04-16-2019;protocol=https"

S = "${WORKDIR}/git"

PV = "v2016.07+git${SRCPV}"
