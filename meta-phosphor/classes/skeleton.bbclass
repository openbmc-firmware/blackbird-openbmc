inherit skeleton-rev

HOMEPAGE = "https://scm.raptorcs.com/scm/git/blackbird-skeleton"

SRC_URI += "${SKELETON_URI}"
S = "${WORKDIR}/git/${SKELETON_DIR}"
