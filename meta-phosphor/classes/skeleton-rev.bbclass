SRCREV ?= "ab90fb2e302412cd539fedc81a2521da5355b52b"
SKELETON_URI ?= "git://scm.raptorcs.com/scm/git/blackbird-skeleton;protocol=https;branch=04-16-2019"
LICENSE = "Apache-2.0"
LIC_FILES_CHKSUM = "file://${WORKDIR}/git/LICENSE;md5=e3fc50a88d0a364313df4b21ef20c29e"
