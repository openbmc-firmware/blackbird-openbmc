MRW_API_SRC_URI ?= "git://scm.raptorcs.com/scm/git/serverwiz;protocol=https"
MRW_API_SRCREV ?= "60c8e10cbb11768cd1ba394b35cb1d6627efec42"

MRW_TOOLS_SRC_URI ?= "git://scm.raptorcs.com/scm/git/phosphor-mrw-tools;protocol=https"
MRW_TOOLS_SRCREV ?= "5dd783bfab6c73851fa31d50e6184a9511f20bcd"
